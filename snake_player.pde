import java.util.Date;

ArrayList<Integer> keyHist = new ArrayList<Integer>();
ArrayList<Point> foodHist = new ArrayList<Point>();

ArrayList<Point> snake = new ArrayList<Point>();
ArrayList<Point> snakeHist = new ArrayList<Point>();

int gridSize = 8;
int gridScale = 40;

// Program modes:
//   0 - Record
//   1 - Playback
int mode = 0;

// Vars for playback mode
int curKey = 0;
int curFood = 0;
boolean finished = false;

void setup(){
	size(640,640); // gridSize*gridScale
	strokeCap(PROJECT);
	
	resetGame();
	placeFood();
	showHelp();
}

void draw(){
	background(255);
	
	float offsetX = (width-gridSize*gridScale)*0.5;
	float offsetY = (height-gridSize*gridScale)*0.5;
	
	// Gridlines
	stroke(200);
	strokeWeight(1);
	for (int i = 0; i <= gridSize*gridScale; i += gridScale){
		line(offsetX+i, offsetY, offsetX+i, offsetY+gridSize*gridScale);
		line(offsetX, offsetY+i, offsetX+gridSize*gridScale, offsetY+i);
	}
	
	// Food
	stroke(245,70,124);
	strokeWeight(gridScale*.75);
	Point food = new Point(-1,-1);
	if (mode == 0) food = foodHist.get(foodHist.size()-1);
	if (mode == 1 && curFood < foodHist.size()) food = foodHist.get(curFood);
	if (curFood < foodHist.size() && !food.equals(new Point(-1,-1))){
		point(offsetX + food.x*gridScale + gridScale*.5,
		      offsetY + food.y*gridScale + gridScale*.5);
	}
	
	// Snaaaaaake!
	stroke(104,255,67);
	for (int i = 1; i < snake.size(); i++){
		line(
			offsetX + snake.get(i-1).x*gridScale + gridScale*.5,
			offsetY + snake.get(i-1).y*gridScale + gridScale*.5,
			offsetX + snake.get(i).x*gridScale + gridScale*.5,
			offsetY + snake.get(i).y*gridScale + gridScale*.5
		);
	}
	
	if (mode == 1 && !finished){
		update();
		frameRate(calcRate(curFood));
	}
}

void update(){
	int lastKey = -1;
	if (mode == 0) lastKey = keyHist.get(keyHist.size()-1);
	if (mode == 1) lastKey = keyHist.get(curKey);
	
	Point last = snake.get(snake.size()-1);
	Point next;
	
	switch (lastKey){
		case UP:
			next = new Point(last.x, last.y-1);
			break;
	 	case DOWN:
			next = new Point(last.x, last.y+1);
			break;
	 	case LEFT:
			next = new Point(last.x-1, last.y);
			break;
		case RIGHT:
			next = new Point(last.x+1, last.y);
			break;
		default:
			next = last;
	}
	
	if (mode == 0 && ((contains(snake, next) && !next.equals(snake.get(0))) || next.x < 0 || next.x >= gridSize || next.y < 0 || next.y >= gridSize)){
		println("Illegal move!");
		keyHist.remove(keyHist.size()-1);
		return;
	}
	snake.add(next);
	
	Point cur = snake.get(snake.size()-1);
	Point food = new Point(-1, -1);
	if (mode == 0) food = foodHist.get(foodHist.size()-1);
	if (mode == 1) food = foodHist.get(curFood);
	if (cur.equals(food)){
		if (mode == 0) placeFood();
		if (mode == 1) curFood++;
	} else {
		if (mode == 0) snakeHist.add(snake.get(0));
		snake.remove(0);
	}
	
	if (mode == 1){
		if (curKey >= keyHist.size()-1 || snake.size() == sq(gridSize)){
			println("Finished!");
			finished = true;
		} else {
			curKey++;
		}
	}
}

void placeFood(){
	if (snake.size() == sq(gridSize)){
		println("Congratulations!");
		finished = true;
		saveFile();
		exit();
	} else {
		ArrayList<Point> free = new ArrayList<Point>();
		Point last;
		if (foodHist.size() > 0){
			last = foodHist.get(foodHist.size()-1);
		} else {
			last = new Point(-1,-1);
		}
		for (int i = 0; i < gridSize; i++){
			for (int j = 0; j < gridSize; j++){
				Point point = new Point(i,j);
				if (!contains(snake, point)) free.add(point);
			}
		}
		foodHist.add(free.get(floor(random(free.size()))));
	}
}

float calcRate(int x){
	// starts at 1fps
	// increases logarithmically
	// scaled depending on the grid size
	return (log(x+1)+1)*(log(gridSize)/2);
}

void keyPressed(){
	if (mode == 0){
		if (key == CODED && (keyCode == UP || keyCode == DOWN || keyCode == LEFT || keyCode == RIGHT)){
			keyHist.add(keyCode);
			update();
		}
		if (key == ' '){
			keyHist.add(keyHist.get(keyHist.size()-1)); // repeat the last key
			update();
		}
		if (key == 'z') undo();
		if (key == 's') saveFile();
		
		if ((key == '=' || key == '+') && gridSize < 16) gridSize++;
		if (key == '-' && gridSize > 3) gridSize--;
	}
	
	if (key == 'n'){
		mode = 0;
		resetGame();
		placeFood();
	}
	if (key == 'o'){
		try {
			selectInput("Select a recording to play back", "loadSave");
		} catch(Exception e) {
			println("Unexpected error when loading file!");
			return;
		}
	}
	if (key == 'h') showHelp();
}

void undo(){
	if (keyHist.size() < 1){
		println("Nothing to undo!");
		return;
	}
	Point cur = snake.get(snake.size()-1);
	Point restored = snakeHist.get(snakeHist.size()-1);
	
	if (foodHist.size() < 2 || !cur.equals(foodHist.get(foodHist.size()-2))){
		// put tail back
		snake.add(0, restored);
		snakeHist.remove(snakeHist.size()-1);
	} else {
		foodHist.remove(foodHist.size()-1);
	}
	keyHist.remove(keyHist.size()-1);
	snake.remove(snake.size()-1);
}

void saveFile(){
	Date d = new Date();
	long now = d.getTime()/1000;
	
	String[] out = new String[3];
	out[0] = String.valueOf(gridSize);
	out[1] = "";
	for (int i = 0; i < keyHist.size(); i++){
		out[1] += keyHist.get(i) + " ";
	}
	out[2] = "";
	for (int i = 0; i < foodHist.size(); i++){
		out[2] += foodHist.get(i).toString() + " ";
	}
	
	saveStrings("out/snake_"+now+".txt", out);
	println("Saved file in: out/snake_"+now+".txt");
}

void loadSave(File file){
	if (file == null){
		println("No file selected!");
		return;
	}
	
	mode = 1;
	
	String[] lines = loadStrings(file.getAbsolutePath());
	
	// gridSize on 0th line
	gridSize = Integer.parseInt(lines[0]);

	resetGame();
	
	// list of keystrokes on 1st line
	String[] keyList = lines[1].split(" ");
	for (int i = 0; i < keyList.length; i++){
		keyHist.add(Integer.parseInt(keyList[i]));
	}
	
	// list of food positions on 2nd line
	String[] foodList = lines[2].split(" ");
	for (int i = 0; i < foodList.length; i+=2){
		foodHist.add(new Point(
			Integer.parseInt(foodList[i]),
			Integer.parseInt(foodList[i+1])
		));
	}
	
	println("Imported", file, "successfully!");
}

void resetGame(){
	keyHist = new ArrayList<Integer>();
	foodHist = new ArrayList<Point>();

	snake = new ArrayList<Point>();
	snakeHist = new ArrayList<Point>();
	
	snake.add(new Point(ceil(gridSize/2.0-2), floor(gridSize/2)));
	snake.add(new Point(ceil(gridSize/2.0-1), floor(gridSize/2)));
	
	curKey = 0;
	curFood = 0;
	finished = false;
	
	frameRate(30);
}

void showHelp(){
	println("SNAKE PLAYER");
	println("Record a game of snake at your own pace and play it back at regular speed!");
	println("");
	println("Controls:");
	println("Move the snake with arrow keys.");
	println("Repeat last move with spacebar.");
	println("Undo previous move with Z.");
	println("Export recording with S. Will automatically export when game is finished.");
	println("Start over by pressing N (new game).");
	println("Press +/- to increase/decrease grid size. (minimum 3x3, maximum 16x16)");
	println("Import a file to play back with O. You can then go back to recording mode with N.");
	println("Show this list of commands again by pressing H.");
	println("");
	println("Have fun!");
	println("");
}


boolean contains(ArrayList<Point> list, Point point){
	for (int i = 0; i < list.size(); i++){
		if (point.equals(list.get(i))){
			return true;
		}
	}
	return false;
}

class Point {
	public int x, y;

	public Point(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public String toString(){
		return x + " " + y;
	}
	
	public boolean equals(Point b){
		return this.x == b.x && this.y == b.y;
	}
}
