// p5js version for openprocessing.org

var keyHist = [];
var foodHist = [];

var snake = [];
var snakeHist = [];

var gridSize = 8;
var gridScale = 40;

// Program modes:
//   0 - Record
//   1 - Playback
var mode = 0;

// Vars for playback mode
var input;
var curKey = 0;
var curFood = 0;
var finished = false;

function setup(){
	input = createFileInput(loadSave);
	
	createCanvas(gridSize*gridScale, gridSize*gridScale);
	strokeCap(PROJECT);
	
	resetGame();
	placeFood();
	showHelp();
}

function draw(){
	background(255);
	
	var offsetX = (width-gridSize*gridScale)*0.5;
	var offsetY = (height-gridSize*gridScale)*0.5;
	
	// Gridlines
	stroke(200);
	strokeWeight(1);
	for (var i = 0; i <= gridSize*gridScale; i += gridScale){
		line(offsetX+i, offsetY, offsetX+i, offsetY+gridSize*gridScale);
		line(offsetX, offsetY+i, offsetX+gridSize*gridScale, offsetY+i);
	}
	
	// Food
	fill(245,70,124);
	noStroke();
	var food = new Point(-1, -1);
	if (mode == 0) food = foodHist[foodHist.length-1];
	if (mode == 1 && curFood < foodHist.length) food = foodHist[curFood];
	if (curFood < foodHist.length && !food.equals(new Point(-1,-1))){
		square(offsetX + food.x*gridScale + gridScale*0.125,
	         offsetY + food.y*gridScale + gridScale*0.125,
		       gridScale*0.75);
	}
	
	// Snaaaaaake!
	stroke(104,255,67);
	strokeWeight(gridScale*0.75);
	for (var i = 1; i < snake.length; i++){
		line(
			offsetX + snake[i-1].x*gridScale + gridScale*0.5,
			offsetY + snake[i-1].y*gridScale + gridScale*0.5,
			offsetX + snake[i].x*gridScale + gridScale*0.5,
			offsetY + snake[i].y*gridScale + gridScale*0.5
		);
	}
	
	if (mode == 1 && !finished){
		update();
		frameRate(calcRate(curFood));
	}
}

function update(){
	var lastKey = -1;
	if (mode == 0) lastKey = keyHist[keyHist.length-1];
	if (mode == 1) lastKey = keyHist[curKey];
	
	var last = snake[snake.length-1];
	var next;
	
	switch (lastKey){
		case UP_ARROW:
			next = new Point(last.x, last.y-1);
			break;
	 	case DOWN_ARROW:
			next = new Point(last.x, last.y+1);
			break;
	 	case LEFT_ARROW:
			next = new Point(last.x-1, last.y);
			break;
		case RIGHT_ARROW:
			next = new Point(last.x+1, last.y);
			break;
		default:
			next = last;
	}
	
	if (mode == 0 && ((contains(snake, next) && !next.equals(snake[0])) || next.x < 0 || next.x >= gridSize || next.y < 0 || next.y >= gridSize)){
		print("Illegal move!");
		keyHist.pop();
		return;
	}
	snake.push(next);
	
	var cur = snake[snake.length-1];
	var food = new Point(-1, -1);
	if (mode == 0) food = foodHist[foodHist.length-1];
	if (mode == 1) food = foodHist[curFood];
	if (cur.equals(food)){
		if (mode == 0) placeFood();
		if (mode == 1) curFood++;
	} else {
		if (mode == 0) snakeHist.push(snake[0]);
		snake.shift();
	}
	
	if (mode == 1){
		if (curKey >= keyHist.length-1 || snake.length == sq(gridSize)){
			print("Finished!");
			finished = true;
		} else {
			curKey++;
		}
	}
}

function placeFood(){
	if (snake.length == sq(gridSize)){
		print("Congratulations!");
		finished = true;
		saveFile();
		return;
	} else {
		var free = [];
		var last;
		if (foodHist.length > 0){
			last = foodHist[foodHist.length-1];
		} else {
			last = new Point(-1,-1);
		}
		for (var i = 0; i < gridSize; i++){
			for (var j = 0; j < gridSize; j++){
				var point = new Point(i,j);
				if (!contains(snake, point)) free.push(point);
			}
		}
		foodHist.push(free[floor(random(free.length))]);
	}
}

function calcRate(x){
	// starts at 1fps
	// increases logarithmically
	// scaled depending on the grid size
	return (log(x+1)+1)*(log(gridSize)/2);
}

function keyPressed(){
	if (mode == 0){
		if (keyCode == UP_ARROW || keyCode == DOWN_ARROW || keyCode == LEFT_ARROW || keyCode == RIGHT_ARROW){
			keyHist.push(keyCode);
			update();
		}
		if (key == ' '){
			keyHist.push(keyHist[keyHist.length-1]); // duplicate the last item
			update();
		}
		if (key == 'z') undo();
		if (key == 's') saveFile();
		if ((key == '=' || key == '+') && gridSize < 16){
			gridSize++;
			resizeCanvas(gridSize*gridScale, gridSize*gridScale);
		}
		if (key == '-' && gridSize > 3){
			gridSize--;
			resizeCanvas(gridSize*gridScale, gridSize*gridScale);
		}
	}
	
	if (key == 'n'){
		mode = 0;
		resetGame();
		placeFood();
	}
	if (key == 'h') showHelp();
}

function undo(){
	if (keyHist.length < 1){
		print("Nothing to undo!");
		return;
	}
	var cur = snake[snake.length-1];
	var restored = snakeHist[snakeHist.length-1];
	
	if (foodHist.length < 2 || !cur.equals(foodHist[foodHist.length-2])){
		// put tail back
		snake.splice(0,0, restored);
		snakeHist.pop();
	} else {
		foodHist.pop();
	}
	keyHist.pop();
	snake.pop();
}

function saveFile(){
	var now = Date.now();
	
	var out = [];
	out[0] = gridSize.toString();
	out[1] = "";
	for (var i = 0; i < keyHist.length; i++){
		out[1] += keyHist[i] + " ";
	}
	out[2] = "";
	for (var i = 0; i < foodHist.length; i++){
		out[2] += foodHist[i] + " ";
	}
	
	saveStrings(out, "snake_"+now+".txt");
	print("Saved file as 'snake_"+now+".txt'");
}

function loadSave(file){
	if (file.type != 'text'){
		print("Invalid file type!");
		return;
	}
	
	mode = 1;
	
	var lines = file.data.split(/\r?\n/);
	
	// gridSize on 0th line
	gridSize = parseInt(lines[0]);
	resizeCanvas(gridSize*gridScale, gridSize*gridScale);
	resetGame();
	
	// list of keystrokes on 1st line
	keyHist = lines[1].split(/\s+/).map(i => parseInt(i));
	
	// list of food positions on 2nd line
	var foodList = lines[2].split(/\s+/);
	for (var i = 0; i < foodList.length; i+=2){
		foodHist.push(new Point(
			parseInt(foodList[i]),
			parseInt(foodList[i+1])
		));
	}
	
	print("Imported "+file.name+" successfully!");
}

function resetGame(){
	keyHist = [];
	foodHist = [];

	snake = [];
	snakeHist = [];
	
	snake.push(new Point(ceil(gridSize/2-2), floor(gridSize/2)));
	snake.push(new Point(ceil(gridSize/2-1), floor(gridSize/2)));
	
	curKey = 0;
	curFood = 0;
	finished = false;
	
	frameRate(30);
}

function showHelp(){
	print("SNAKE PLAYER");
	print("Record a game of snake at your own pace and play it back at regular speed!");
	print("");
	print("Controls:");
	print("Move the snake with arrow keys.");
	print("Repeat last move with spacebar.");
	print("Undo previous move with Z.");
	print("Export recording with S. Will automatically export when game is finished.");
	print("Start over by pressing N (new game).");
	print("Press +/- to increase/decrease grid size. (minimum 3x3, maximum 16x16)");
	print("Import a file to play back with the dialog below. You can then go back to recording mode with N.");
	print("Show this list of commands again by pressing H.");
	print("");
	print("Have fun!");
	print("");
}


function contains(list, point){
	for (var i = 0; i < list.length; i++){
		if (point.equals(list[i])){
			return true;
		}
	}
	return false;
}

class Point {
	constructor(x, y){
		this.x = x;
		this.y = y;
	}
	
	toString(){
		return this.x + " " + this.y;
	}
	
	equals(b){
		return this.x == b.x && this.y == b.y;
	}
}
