# Snake Player
Record a game of snake at your own pace and play it back at regular speed!

https://www.openprocessing.org/sketch/936926

Sample save files are located in [`samples/`](samples/) which you can view in either version.

## Controls:
* Move the snake with arrow keys.
* Repeat last move with spacebar.
* Undo previous move with Z.
* Export recording with S. Will automatically export when game is finished.
* Start over by pressing N (new game).
* Press +/- to increase/decrease grid size. (minimum 3x3, maximum 16x16)
* Import a file to play back with O (java) or using the dialog (openprocessing). You can then go back to recording mode with N.
* Show this list of commands by pressing H.
